FROM python:3.10-slim

WORKDIR /app

COPY . /app/python-challenge

WORKDIR /app/python-challenge

RUN pip install poetry &&\
    poetry install

ENTRYPOINT ["poetry", "run", "pychallenge"]
