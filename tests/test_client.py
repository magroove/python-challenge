import pytest
import requests

from python_challenge import client, settings
from python_challenge.models import Article


@pytest.mark.vcr()
def test_get_articles():
    articles = (
        Article(
            title=(
                "Kenya’s new tennis star made history at the Australian "
                "Open"
            ),
            link=(
                "https://qz.com/africa/2118339/australian-open-kenyas-"
                "tennis-star-angela-okutoyi-made-history/"
            ),
        ),
        Article(
            title=(
                "African diplomats are live-streaming and making deliveries"
                " to China’s consumers"
            ),
            link=(
                "https://qz.com/africa/2117788/african-nations-bet-big-on-"
                "chinas-e-commerce-market/"
            ),
        ),
        Article(
            title="What’s causing recent coups in west Africa?",
            link=(
                "https://qz.com/africa/2117845/what-has-caused-three-coups-"
                "in-two-years-in-west-africa/"
            ),
        ),
    )

    for page in client.Client.get_articles(no_articles_per_page=3):
        for index, article in enumerate(page):
            assert article.title == articles[index].title


@pytest.mark.vcr()
def test_get_articles_http_error():
    with pytest.raises(requests.exceptions.RequestException):
        client.API_URL = "https://example.example"

        pages = client.Client.get_articles()

        for x in pages:
            pass


@pytest.mark.vcr()
def test_get_articles_key_error():
    """
    The corresponding cassette doesn't include the keys expected by the method.
    """
    client.API_URL = settings.API_URL

    with pytest.raises(KeyError):
        for x in client.Client.get_articles():
            pass
