# python challenge

Magroove internship program challenge.

The project includes:

- Code **linting** and **PEP8 compliance** + **pre-commit** settings.
- Containerzation (**Docker**).
- 5 stages **CI/CD**: tests, build, upload, release and deploy.
- Kubernetes templates.
- A command-line interface.

## Development

- [Git](https://git-scm.com/)
- [Python](https://python.org)
- [Poetry](https://python-poetry.org)*
- [Docker](https://docker.com) (optional)
- [Kubernetes](http://kubernetes.io) (optional)

## Relevant links

- [Kubernetes templates](https://gitlab.com/magroove/python-challenge/-/snippets)

## Installation

Clone the repository inside your projects folder:

```
git clone https://gitlab.com/magroove/python-challenge.git
cd python-challenge
```

Install the dependencies:

```
poetry install
```

## Usage

Create a `.env` file:

```
echo "API_URL=https://content.qz.com/graphql/" > .env
```

Run the application:

```
poetry run pychallenge
```

For more options:

```
poetry run pychallenge --help
```

---

*The package distributed at the [Releases](https://gitlab.com/magroove/python-challenge/-/releases) page can be installed via `pip`.
