import requests
from urllib3.exceptions import HTTPError

from .enums import EditionEnum, OperationEum
from .models import Article, Query
from .pc_logging import get_logger
from .settings import API_URL

logger = get_logger()


class Client:
    @staticmethod
    def get_articles(
        no_pages=1,
        operation=OperationEum.LATEST_ARTICLES,
        edition=EditionEnum.AFRICA,
        offset="",
        no_articles_per_page=10,
    ):
        """
        Iterates through JSON returned by the API.

        Args:
            offset (str): key of the precedent page.
            no_articles_per_page (int): number of articles per page.
            no_pages (int): number of pages.
            operation (Enum): corresponding operation enumerator.
            edition (Enum): corresponding edition enumerator.

        Yields:
            map(Article): a map of Article objects.

        Raises:
            HTTPError: if the API is unaccessible.
            KeyError: if the JSON returned doesn't match the format expected.
        """

        for _ in range(no_pages):
            query = Query(
                operation_name=operation.value,
                variables={
                    "edition": edition.value,
                    "postsPerPage": no_articles_per_page,
                    "after": offset,
                },
            )

            try:
                posts_key = requests.post(API_URL, json=query.__dict__).json()[
                    "data"
                ]["posts"]

                nodes = posts_key["nodes"]

                page_info = posts_key["pageInfo"]

                yield map(lambda x: Article(x["title"], x["link"]), nodes)

                if page_info["hasNextPage"] is False:  # pragma: no cover
                    break

                offset = page_info["endCursor"]

            except (HTTPError, requests.exceptions.RequestException) as e:
                logger.error(e)
                raise

            except KeyError as e:
                logger.error(e)
                raise
