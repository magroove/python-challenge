class Article:
    title = None
    link = None

    def __init__(self, title, link):
        self.title = title
        self.link = link


class Query:
    operation_name = None
    variables = None
    extensions = None

    def __init__(self, operation_name, variables):
        self.operation_name = operation_name
        self.variables = variables
        self.extensions = {
            "persistedQuery": {
                "version": 1,
                "sha256Hash": "319d7f8d6d9092be2db479d250c989b6963ce52baa3a724"
                "ec7e73642265a5e91",
            }
        }
